package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Roles;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface VariablesDao extends CrudRepository<Variables, Long> {

	public Variables findById(long id);

	public Variables findByIdAndIsDeleted(long id, boolean isDeleted);

	public Variables findByMappingAndIsDeleted(String mapping, boolean isDelete);

	public Variables findByMappingAndRoleAndIsDeleted(String mapping, Roles role, boolean isDeleted);

}
