package com.kuliza.lending.backoffice.exceptions;

public class VariableNotFoundException extends RuntimeException {

	public VariableNotFoundException() {
		super();
	}

	public VariableNotFoundException(String message) {
		super(message);
	}
}