package com.kuliza.lending.backoffice.exceptions;

public class TabNotConfiguredForRoleException extends RuntimeException {

	public TabNotConfiguredForRoleException() {
		super();
	}

	public TabNotConfiguredForRoleException(String message) {
		super(message);
	}

}
