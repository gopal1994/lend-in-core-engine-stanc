package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_headers", uniqueConstraints = { @UniqueConstraint(columnNames = { "headerKey", "card_id" }) })
public class Headers extends BaseModel {

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String headerKey;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "card_id")
	private Cards card;

	@Column(nullable = false)
	private int headerOrder;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "header", orphanRemoval = true)
	@OrderBy("headerVariableOrder ASC")
	private Set<HeadersVariablesMapping> headersVariables;

	public Headers() {
		super();
		this.setIsDeleted(false);
	}

	public Headers(String label, String headerKey, Cards card, int headerOrder) {
		super();
		this.label = label;
		this.headerKey = headerKey;
		this.card = card;
		this.headerOrder = headerOrder;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getHeaderKey() {
		return headerKey;
	}

	public void setHeaderKey(String headerKey) {
		this.headerKey = headerKey;
	}

	public Set<HeadersVariablesMapping> getHeadersVariables() {
		return headersVariables;
	}

	public Cards getCard() {
		return card;
	}

	public void setCard(Cards card) {
		this.card = card;
	}

	public int getHeaderOrder() {
		return headerOrder;
	}

	public void setHeaderOrder(int headerOrder) {
		this.headerOrder = headerOrder;
	}

	public void setHeadersVariables(Set<HeadersVariablesMapping> headersVariables) {
		this.headersVariables = headersVariables;
	}

}
