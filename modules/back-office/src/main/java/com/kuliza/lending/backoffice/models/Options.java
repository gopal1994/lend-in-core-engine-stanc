package com.kuliza.lending.backoffice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_variable_options", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "optionKey", "variable_id" }) })
public class Options extends BaseModel {

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String optionKey;

	@Column(nullable = false)
	private int optionOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "variable_id")
	private Variables variable;

	public Options() {
		super();
		this.setIsDeleted(false);
	}

	public Options(String label, String optionKey, int optionOrder, Variables variable) {
		super();
		this.label = label;
		this.optionKey = optionKey;
		this.optionOrder = optionOrder;
		this.variable = variable;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getOptionKey() {
		return optionKey;
	}

	public void setOptionKey(String optionKey) {
		this.optionKey = optionKey;
	}

	public int getOptionOrder() {
		return optionOrder;
	}

	public void setOptionOrder(int optionOrder) {
		this.optionOrder = optionOrder;
	}

	public Variables getVariable() {
		return variable;
	}

	public void setVariable(Variables variable) {
		this.variable = variable;
	}

}
