package com.kuliza.lending.configurator.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class UpdateExpression {

	private String productId;
	private String groupId;
	private String userId;
	private String expressionId;

	@NotNull(message = "newExpression is a required key")
	@Valid
	private NewExpression newExpression;

	public UpdateExpression() {
		this.newExpression = new NewExpression();
	}

	public UpdateExpression(NewExpression newExpression) {
		this.newExpression = newExpression;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getExpressionId() {
		return expressionId;
	}

	public void setExpressionId(String expressionId) {
		this.expressionId = expressionId;
	}

	public NewExpression getNewExpression() {
		return newExpression;
	}

	public void setNewExpression(NewExpression newExpression) {
		this.newExpression = newExpression;
	}

	// @Override
	// public String toString() {
	// StringBuilder updateVariable = new StringBuilder();
	// updateVariable.append("{ ");
	// updateVariable.append("newExpression : " + newExpression.toString());
	// updateVariable.append(" }");
	// return updateVariable.toString();
	//
	// }
}
