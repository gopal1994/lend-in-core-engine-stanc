package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.configurator.serializers.BulkRuleSerializer;
import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@JsonFilter("groupFilter")
@Entity
@Table(name = "ce_group", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "productId" }) })
@Where(clause = "is_deleted=0")
public class Group extends BaseModelWithName {

	@Where(clause = "is_deleted=0")
	@OrderBy(clause = "id ASC")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
	private Set<Expression> expressions;

	@Where(clause = "is_deleted=0")
	@JsonSerialize(converter = BulkRuleSerializer.class)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
	@OrderBy(clause = "id ASC")
	private Set<Rule> rules;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	public Group() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Group(long id, String name, Product product) {
		super();
		this.setId(id);
		this.setName(name);
		this.product = product;
	}

	public Group(String name, Product product) {
		this.setName(name);
		this.product = product;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	@JsonIgnore
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Set<Expression> getExpressions() {
		return expressions;
	}

	public void setExpressions(Set<Expression> expressions) {
		this.expressions = expressions;
	}

	public Set<Rule> getRules() {
		return rules;
	}

	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}
}
