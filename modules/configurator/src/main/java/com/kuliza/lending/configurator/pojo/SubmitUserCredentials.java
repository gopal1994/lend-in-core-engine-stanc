package com.kuliza.lending.configurator.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class SubmitUserCredentials {

	@NotNull(message = "userName is a required key")
	@NotEmpty(message = "User Name cannot be Empty")
	String userName;

	@NotNull(message = "Password is a required key")
	@NotEmpty(message = "Password cannot be Empty")
	String password;

	public SubmitUserCredentials() {
		this.userName = "";
		this.password = "";
	}

	public SubmitUserCredentials(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("userName : " + userName + ", ");
	// inputData.append("password : " + password);
	// inputData.append(" }");
	// return inputData.toString();
	//
	// }

}
