package com.kuliza.lending.configurator.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "ce_execution")
public class Execution extends BaseModel {

	@Column(nullable = false)
	private String identifier;
	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private boolean isTestOrProduction;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId")
	private Product product;
	@Lob
	@Column(nullable = false)
	private String input;
	@Lob
	@Column(nullable = false)
	private String output;

	public Execution() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Execution(String identifier, String input, String output, Product product, Boolean isTestOrProduction) {
		this.identifier = identifier;
		this.input = input;
		this.output = output;
		this.product = product;
		this.isTestOrProduction = isTestOrProduction;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public boolean isTestOrProduction() {
		return isTestOrProduction;
	}

	public void setTestOrProduction(boolean isTestOrProduction) {
		this.isTestOrProduction = isTestOrProduction;
	}

}
