package com.kuliza.lending.configurator.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.RegisterUser;
import com.kuliza.lending.configurator.utils.Constants;

@RestController
public class UserControllers {

	private static final Logger logger = LoggerFactory.getLogger(UserControllers.class);

	@Autowired
	KeyCloakManager keycloakManager;

	@Autowired
	private UserDao userDao;

	@Autowired
	private KeyCloakService keyclaokService;

	// API to create new user
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	@Transactional
	public Object userSubmit(@RequestBody @Valid RegisterUser input, BindingResult result, HttpServletRequest request) {
		logger.info("-->Entering user Submit()");
		try {
			logger.debug("creating user with  emailId: " + input.getEmailId() + " role:" + input.getRole());
			UserRepresentation userRepresentation = keyclaokService.createUserWithRole(input.getEmailId(),
					input.getPassword(), input.getRole());
			if (userRepresentation != null) {
				User user = userDao.findByUserName(input.getEmailId());
				if (user == null) {
					user = new User(input.getEmailId());
					user = userDao.save(user);

				}
				logger.debug("user created successfully");
				UserResource userResource = keyclaokService.getUserResource(userRepresentation.getId());
				Map<String, List<String>> attribute = new HashMap<>();
				List<String> userIdList = new ArrayList<>();
				userIdList.add(String.valueOf(user.getId()));
				attribute.put("user_id", userIdList);
				userRepresentation.setAttributes(attribute);
				userResource.update(userRepresentation);
				logger.debug("added custom attribute for user");

			}
			logger.info("<-- Exiting user Submit()");
			return userRepresentation;
		} catch (Exception e) {
			logger.error("error while user registration");
			e.printStackTrace();
			return new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, "error while register", e);
		}

	}

}
