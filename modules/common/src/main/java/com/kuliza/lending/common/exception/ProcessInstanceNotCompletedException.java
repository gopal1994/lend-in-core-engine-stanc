package com.kuliza.lending.common.exception;

public class ProcessInstanceNotCompletedException extends Exception {

	public ProcessInstanceNotCompletedException(String processInstanceId) {
		super("This process instance : " + processInstanceId
				+ " is not ended. Please use runtime services to query any data.");
	}

}
