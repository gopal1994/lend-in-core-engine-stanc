package com.kuliza.lending.authorization.exception;

public class MockException extends RuntimeException {

	private static final long serialVersionUID = 2021772623711343387L;

	public MockException(String s) {
		super(s);
	}
}
