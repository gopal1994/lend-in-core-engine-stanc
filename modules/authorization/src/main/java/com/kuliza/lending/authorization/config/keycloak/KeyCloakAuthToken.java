package com.kuliza.lending.authorization.config.keycloak;

import java.util.Collection;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class KeyCloakAuthToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = -4392699141437432108L;
	
	private String user_Id;

	public String getUser_Id() {
		return user_Id;
	}

	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}

	public KeyCloakAuthToken(Object token) {
		super(null, token);
	}

	public KeyCloakAuthToken(Object principal, Object token, Collection<? extends GrantedAuthority> authorities) {
		super(principal, token, authorities);
	}

}
