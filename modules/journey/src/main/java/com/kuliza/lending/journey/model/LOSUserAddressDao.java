package com.kuliza.lending.journey.model;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface LOSUserAddressDao extends CrudRepository<LOSUserAddressModel, Long> {

	public LOSUserAddressModel findById(long id);

	public LOSUserAddressModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<LOSUserAddressModel> findByLosUserModel(LOSUserModel user);

	public LOSUserAddressModel findByLosUserModelAndAddressType(LOSUserModel user, String addressType);
}
