package com.kuliza.lending.journey.customer_dashboard.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerDashboardApplicationDisbursed {
	String loanAmount;
	String emi;
	String remainingTenure;
	String tenure;
	String nextRepaymentDate;
	String outstandingLoanAmount;
	String totalOverdueLoanAmount;
	String overdueLoanAmount;
	String overdueFeeAmount;
	List<Object> paymentHistory;
	List<Object> repaymentInfo;
	String applicationDate;
	String applicationStatus;
	String earlyTerminationAmount;
	String totalPaymentAmount;
	String noOfMonthsOverdue;
	String loanContractNumber;
	String loanContractUrl;
	String disbursementDate;
	String disbursementMethod;
	String repaymentScheduleUrl;

	public CustomerDashboardApplicationDisbursed() {
		super();
		this.loanAmount = "";
		this.emi = "";
		this.remainingTenure = "";
		this.tenure = "";
		this.nextRepaymentDate = "";
		this.outstandingLoanAmount = "";
		this.totalOverdueLoanAmount = "";
		this.overdueLoanAmount = "";
		this.overdueFeeAmount = "";
		this.loanContractUrl = "";
		this.paymentHistory = new ArrayList<>();
		this.repaymentInfo = new ArrayList<>();
		this.applicationDate = "";
		this.applicationStatus = "";
		this.earlyTerminationAmount = "";
		this.totalPaymentAmount = "";
		this.noOfMonthsOverdue = "";
		this.loanContractNumber = "";
		this.disbursementDate = "";
		this.disbursementMethod = "";
		this.repaymentScheduleUrl = "";
	}

	public CustomerDashboardApplicationDisbursed(String loanAmount, String emi, String remainingTenure, String tenure,
			String nextRepaymentDate, String outstandingLoanAmount, String totalOverdueLoanAmount,
			String overdueLoanAmount, String overdueFeeAmount, List<Object> paymentHistory, List<Object> repaymentInfo,
			String applicationDate, String applicationStatus, String earlyTerminationAmount, String totalPaymentAmount,
			String noOfMonthsOverdue, String loanContractNumber, String loanContractUrl, String disbursementDate,
			String disbursementMethod, String repaymentScheduleUrl) {
		super();
		this.loanAmount = loanAmount;
		this.emi = emi;
		this.remainingTenure = remainingTenure;
		this.tenure = tenure;
		this.nextRepaymentDate = nextRepaymentDate;
		this.outstandingLoanAmount = outstandingLoanAmount;
		this.totalOverdueLoanAmount = totalOverdueLoanAmount;
		this.overdueLoanAmount = overdueLoanAmount;
		this.overdueFeeAmount = overdueFeeAmount;
		this.paymentHistory = paymentHistory;
		this.repaymentInfo = repaymentInfo;
		this.applicationDate = applicationDate;
		this.applicationStatus = applicationStatus;
		this.earlyTerminationAmount = earlyTerminationAmount;
		this.totalPaymentAmount = totalPaymentAmount;
		this.noOfMonthsOverdue = noOfMonthsOverdue;
		this.loanContractNumber = loanContractNumber;
		this.loanContractUrl = loanContractUrl;
		this.disbursementDate = disbursementDate;
		this.disbursementMethod = disbursementMethod;
		this.repaymentScheduleUrl = repaymentScheduleUrl;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEmi() {
		return emi;
	}

	public void setEmi(String emi) {
		this.emi = emi;
	}

	public String getRemainingTenure() {
		return remainingTenure;
	}

	public void setRemainingTenure(String remainingTenure) {
		this.remainingTenure = remainingTenure;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public String getNextRepaymentDate() {
		return nextRepaymentDate;
	}

	public void setNextRepaymentDate(String nextRepaymentDate) {
		this.nextRepaymentDate = nextRepaymentDate;
	}

	public String getOutstandingLoanAmount() {
		return outstandingLoanAmount;
	}

	public void setOutstandingLoanAmount(String outstandingLoanAmount) {
		this.outstandingLoanAmount = outstandingLoanAmount;
	}

	public String getTotalOverdueLoanAmount() {
		return totalOverdueLoanAmount;
	}

	public void setTotalOverdueLoanAmount(String totalOverdueLoanAmount) {
		this.totalOverdueLoanAmount = totalOverdueLoanAmount;
	}

	public String getOverdueLoanAmount() {
		return overdueLoanAmount;
	}

	public void setOverdueLoanAmount(String overdueLoanAmount) {
		this.overdueLoanAmount = overdueLoanAmount;
	}

	public String getOverdueFeeAmount() {
		return overdueFeeAmount;
	}

	public void setOverdueFeeAmount(String overdueFeeAmount) {
		this.overdueFeeAmount = overdueFeeAmount;
	}

	public List<Object> getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(List<Object> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	public List<Object> getRepaymentInfo() {
		return repaymentInfo;
	}

	public void setRepaymentInfo(List<Object> repaymentInfo) {
		this.repaymentInfo = repaymentInfo;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getEarlyTerminationAmount() {
		return earlyTerminationAmount;
	}

	public void setEarlyTerminationAmount(String earlyTerminationAmount) {
		this.earlyTerminationAmount = earlyTerminationAmount;
	}

	public String getTotalPaymentAmount() {
		return totalPaymentAmount;
	}

	public void setTotalPaymentAmount(String totalPaymentAmount) {
		this.totalPaymentAmount = totalPaymentAmount;
	}

	public String getNoOfMonthsOverdue() {
		return noOfMonthsOverdue;
	}

	public void setNoOfMonthsOverdue(String noOfMonthsOverdue) {
		this.noOfMonthsOverdue = noOfMonthsOverdue;
	}

	public String getLoanContractNumber() {
		return loanContractNumber;
	}

	public void setLoanContractNumber(String loanContractNumber) {
		this.loanContractNumber = loanContractNumber;
	}

	public String getLoanContractUrl() {
		return loanContractUrl;
	}

	public void setLoanContractUrl(String loanContractUrl) {
		this.loanContractUrl = loanContractUrl;
	}

	public String getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	public String getDisbursementMethod() {
		return disbursementMethod;
	}

	public void setDisbursementMethod(String disbursementMethod) {
		this.disbursementMethod = disbursementMethod;
	}

	public String getRepaymentScheduleUrl() {
		return repaymentScheduleUrl;
	}

	public void setRepaymentScheduleUrl(String repaymentScheduleUrl) {
		this.repaymentScheduleUrl = repaymentScheduleUrl;
	}

}
