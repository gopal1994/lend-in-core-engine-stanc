package com.kuliza.lending.journey.service;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.common.utils.LogType;
import com.kuliza.lending.journey.model.LOSUserDao;
import com.kuliza.lending.journey.model.LOSUserModel;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.OtpLoginInputform;

@Service
public class OtpServices {

	private static final Logger logger = LoggerFactory.getLogger(OtpServices.class);

	@Autowired
	private LOSUserDao losUserDao;

	@Autowired
	private KeyCloakManager keycloakManager;

	/**
	 * This functions creates returns an existing user with given mobile number or
	 * creates a new user if not already present in table los_user and returns that
	 * user.
	 * 
	 * @param mobileNumber
	 * @return LOSUserModel
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	public LOSUserModel createOrUpdateUser(String mobileNumber) throws Exception {
		LOSUserModel userData = losUserDao.findByContactNumberAndIsDeleted(mobileNumber, false);
		if (userData == null) {
			userData = new LOSUserModel();
			userData.setContactNumber(mobileNumber);
			losUserDao.save(userData);
		}
		return userData;
	}

	/**
	 * This functions generates a OTP and sends it to user's mobile.
	 * 
	 * @param mobile
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse generateOTP(HttpServletRequest request, CustomerLoginInput customerLoginInput) throws Exception {
		try {
			CustomLogger.log(customerLoginInput.getMobile(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
					null, null, "generateOTP : " + customerLoginInput.getMobile());
		} catch (JsonProcessingException e) {
			CustomLogger.logException(customerLoginInput.getMobile(), null, LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, e, "generateOTP : " + customerLoginInput.getMobile());
		}
		ApiResponse apiResponse = null;
		// TODO GENERATE AND SEND OTP
		apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "OTP sent successfully!");
		try {
			CustomLogger.log(customerLoginInput.getMobile(), null, LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
					null, null, apiResponse);
		} catch (JsonProcessingException e) {
			CustomLogger.logException(customerLoginInput.getMobile(), null, LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, e, apiResponse);
		}
		return apiResponse;

	}

	/**
	 * This functions validates the OTP for the given user, creates a new user if
	 * user not already present and returns tokens for the given user.
	 * 
	 * @param otpLoginInputform
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse validateOtpAndCreateOrUpdateUser(HttpServletRequest request, OtpLoginInputform otpLoginInputform)
			throws Exception {
		ApiResponse apiResponse = null;
		try {
			CustomLogger.log(otpLoginInputform.getMobile(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
					null, null, "validateOtpAndCreateOrUpdateUser : " + otpLoginInputform.getMobile());
		} catch (JsonProcessingException e) {
			CustomLogger.logException(otpLoginInputform.getMobile(), null, LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, e,
					"validateOtpAndCreateOrUpdateUser : " + otpLoginInputform.getMobile());
		}
		try {
			CustomLogger.log(Thread.currentThread().getStackTrace()[1].getMethodName(), null,
					LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null, null, otpLoginInputform);

			// TODO Validate OTP
			logger.debug("Creating/Updating user for mobile number : " + otpLoginInputform.getMobile());
			LOSUserModel userData = losUserDao.findByContactNumberAndIsDeleted(otpLoginInputform.getMobile(), false);
			boolean isNewUser = false;
			if (userData == null) {
				isNewUser = true;
				userData = createOrUpdateUser(otpLoginInputform.getMobile());
			}
			if (userData != null) {
				String keycloakId = "user@" + userData.getId() + ".com";
				String password = Base64.encodeBase64String(keycloakId.getBytes());
				ApiResponse registerResponse = null;
				if (isNewUser) {
					try {
						registerResponse = keycloakManager.createUserWithRole(keycloakId, password, "user");

					} catch (Exception e) {
						CustomLogger.logException(Thread.currentThread().getStackTrace()[1].getMethodName(), null,
								LogType.EXCEPTION, JobType.INBOUND_API, null, null, e, registerResponse);
						apiResponse = keycloakManager.loginWithEmailAndPassword(keycloakId, password);

					}
				}
				if (registerResponse == null || registerResponse.getStatus() == 200) {
					apiResponse = keycloakManager.loginWithEmailAndPassword(keycloakId, password);
				} else {
					apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);

				}
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);

			}

		}

		catch (Exception e) {
			CustomLogger.logException(Thread.currentThread().getStackTrace()[1].getMethodName(), null,
					LogType.EXCEPTION, JobType.INBOUND_API, null, null, e, otpLoginInputform);
			apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		try {
			CustomLogger.log(otpLoginInputform.getMobile(), null, LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
					null, null, apiResponse);
		} catch (JsonProcessingException e) {
			CustomLogger.logException(otpLoginInputform.getMobile(), null, LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, e, apiResponse);
		}
		return apiResponse;

	}
}