package com.kuliza.lending.journey.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface LOSUserLoanDataDao
		extends CrudRepository<LOSUserLoanDataModel, Long>, JpaRepository<LOSUserLoanDataModel, Long> {
	public LOSUserLoanDataModel findById(long id);

	public LOSUserLoanDataModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public LOSUserLoanDataModel findByProcessInstanceIdAndIsDeleted(String processInstanceId, boolean isDeleted);

	public List<LOSUserLoanDataModel> findByIsDeleted(boolean isDeleted);

	public List<LOSUserLoanDataModel> findByLosUserModel(LOSUserModel losUserModel);

	public List<LOSUserLoanDataModel> findByLosUserModelOrderByCreatedDesc(LOSUserModel losUserModel);

	public List<LOSUserLoanDataModel> findByLosUserModelOrderByCreatedAsc(LOSUserModel losUserModel);

	public LOSUserLoanDataModel findFirstByLosUserModelOrderByCreatedDesc(LOSUserModel losUserModel);

	public List<LOSUserLoanDataModel> findByLosUserModelAndIsDeleted(LOSUserModel losUserModel, boolean isDeleted);
}
