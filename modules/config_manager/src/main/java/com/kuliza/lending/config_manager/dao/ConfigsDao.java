package com.kuliza.lending.config_manager.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.config_manager.models.ConfigModel;

@Repository
public interface ConfigsDao extends CrudRepository<ConfigModel, Long> {
	
	public ConfigModel findByClientId(String clientId);

}
